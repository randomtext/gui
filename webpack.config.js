const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    context: __dirname,
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'main.js',
        publicPath: '/',
    },
    devServer: {
        historyApiFallback: true,
        port: 9091,
    },
    module: {
        rules: [
            {
                test: /\.(png|j?g|svg|gif|jpeg)?$/,
                use: "file-loader",
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.js$/,
                use: 'babel-loader',
            },
        ],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                default: false,
                vendors: false,

                // vendor chunk
                vendor: {
                    // name of the chunk
                    name: "vendor",

                    // async + async chunks
                    chunks: "all",

                    // import file path containing node_modules
                    test: /node_modules/,

                    // priority
                    priority: 20,
                },

                // common chunk
                common: {
                    name: "common",
                    minChunks: 2,
                    chunks: "all",
                    priority: 10,
                    reuseExistingChunk: true,
                    enforce: true,
                },
            },
        },
    },
    devtool: "source-map",
    resolve: {
        alias: {
            "@gql": path.resolve(__dirname, "src/core/gql"),
            "@utils": path.resolve(__dirname, "src/core/utils"),
            "@assets": path.resolve(__dirname, "assets"),
        },
        extensions: [".js", ".jsx"],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: path.resolve(__dirname, "public/index.html"),
            filename: "index.html",
        }),
    ],
};
