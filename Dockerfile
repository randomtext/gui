FROM nginx:alpine AS ngx_brotli_build
ENV NGX_MODULE_COMMIT 9aec15e2aa6feea2113119ba06460af70ab3ea62
ENV NGX_MODULE_PATH ngx_brotli
ENV MORE_HEADERS_VERSION=0.33
ENV MORE_HEADERS_GITREPO=openresty/headers-more-nginx-module
RUN wget "https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O nginx.tar.gz
RUN wget "https://github.com/google/ngx_brotli/archive/${NGX_MODULE_COMMIT}.tar.gz" -O ${NGX_MODULE_PATH}.tar.gz
RUN wget "https://github.com/${MORE_HEADERS_GITREPO}/archive/v${MORE_HEADERS_VERSION}.tar.gz" -O extra_module.tar.gz
RUN apk add --no-cache --virtual .build-deps \
  gcc \
  libc-dev \
  make \
  openssl-dev \
  pcre-dev \
  zlib-dev \
  linux-headers \
  libxslt-dev \
  gd-dev \
  geoip-dev \
  perl-dev \
  libedit-dev \
  mercurial \
  bash \
  alpine-sdk \
  findutils \
  brotli-dev
RUN mkdir -p /usr/src/extra_module
RUN tar -xzC /usr/src/extra_module -f extra_module.tar.gz
# Reuse same cli arguments as the nginx:alpine image used to build
RUN CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
  tar -zxf nginx.tar.gz && \
  tar -xzf "${NGX_MODULE_PATH}.tar.gz" && \
  cd nginx-$NGINX_VERSION && \
  ./configure --with-compat $CONFARGS --add-dynamic-module="$(pwd)/../${NGX_MODULE_PATH}-${NGX_MODULE_COMMIT}" && \
  make && make install && \
  mkdir /so-deps && cp -L $(ldd /usr/local/nginx/modules/ngx_http_brotli_filter_module.so 2>/dev/null | grep '/usr/lib/' | awk '{ print $3 }' | tr '\n' ' ') /so-deps && \
  ./configure --with-compat $CONFARGS --add-dynamic-module="/usr/src/extra_module/*" && \
  make && make install
RUN openssl dhparam -out /etc/nginx/dhparam.pem 2048
# Build environment
FROM node:14.15.4-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
RUN yarn
COPY . ./
RUN yarn build
FROM nginx:alpine
COPY --from=ngx_brotli_build /etc/nginx/dhparam.pem                                          /etc/nginx/dhparam.pem
COPY --from=ngx_brotli_build /so-deps                                                        /usr/lib
COPY --from=ngx_brotli_build /usr/local/nginx/modules/ngx_http_brotli_filter_module.so       /usr/local/nginx/modules/ngx_http_brotli_filter_module.so
COPY --from=ngx_brotli_build /usr/local/nginx/modules/ngx_http_brotli_static_module.so       /usr/local/nginx/modules/ngx_http_brotli_static_module.so
COPY --from=ngx_brotli_build /usr/local/nginx/modules/ngx_http_headers_more_filter_module.so /usr/local/nginx/modules/ngx_http_headers_more_filter_module.so
RUN adduser -u 82 -D -S -G www-data www-data
RUN mkdir -p /etc/nginx/sites-available
RUN mkdir -p /etc/nginx/sites-enabled
COPY nginx/nginx.conf /etc/nginx/
COPY nginx/general.conf /etc/nginx/
COPY nginx/security.conf /etc/nginx/
COPY nginx/server.conf /etc/nginx/sites-available/
COPY nginx/server.conf /etc/nginx/sites-enabled/
COPY --from=build /app/dist /var/www/gui/public
EXPOSE 443
# Copy .env file and shell script to container
WORKDIR /var/www/gui/public
COPY ./env.sh .
COPY .env .
# Add bash
RUN apk add --no-cache bash
# Make our shell script executable
RUN chmod +x env.sh
CMD ["/bin/bash", "-c", "/var/www/gui/public/env.sh && nginx -g \"daemon off;\""]