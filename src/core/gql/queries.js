import { gql } from "apollo-boost";

export const GET_RANDOM_TEXT = gql`
    query GetRandomText {
        getRandomText {
            message
        }
    }
`;
