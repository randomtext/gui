import { gql } from "apollo-boost";

export const ADD_TEXT = gql`
    mutation AddText(
        $message: String!
        $author_id: String!
    ) {
        addText(
            message: $message
            author_id: $author_id
        ) {
            success
        }
    }
`;