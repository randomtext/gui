import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import App from "../../modules/App";

const Routers = () => (
    <Router>
        <Switch>
            <Route exact path="/">
                <Redirect to="/random" />
            </Route>
            <Route path="/random">
                <App />
            </Route>
        </Switch>
    </Router>
);

export default Routers;
