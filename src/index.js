import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "@apollo/client";
import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import Router from "./core/router";

const apiUrl = window._env_.API_URL;

const client = new ApolloClient({
  cache: new InMemoryCache({}),
  link: new HttpLink({
    uri: apiUrl,
  }),
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Router />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
