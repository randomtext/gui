import React, { useState, useEffect, useRef } from "react";
import { useQuery, useMutation } from "@apollo/client";
import learnImage from "@assets/learn-online.jpeg";
import { GET_RANDOM_TEXT } from "@gql/queries";
import { ADD_TEXT } from "@gql/mutations";
import { generateId } from "@utils";

const App = () => {
  const [authorId] = useState(generateId());
  const [newText, setText] = useState("");
  const [serverText, setServerText] = useState({message: "waiting for server"});
  const [forceRefresh, setForceRefresh] = useState(0);
  const [addText] = useMutation(ADD_TEXT);
  const getRandom = useQuery(GET_RANDOM_TEXT, {
    variables: {},
    fetchPolicy: "network-only",
  });
  const serverUpdates = useRef([]);

  const subscribeUserCoordinates = () => {
    const subColl = "texts";
    const eventSource = new EventSource(
        `${window._env_.SUBSCRIPTION_URL}?collName=${subColl}&authorID=${authorId}`
    );
    eventSource.onmessage = e => {
      const newData = JSON.parse(e.data);
      switch (serverUpdates.current.length) {
        case 0:
          serverUpdates.current = [...serverUpdates.current, newData.text.graphql_status];
          break;
        case 1:
          serverUpdates.current = [...serverUpdates.current, newData.text.rabbitmq_status];
          break;
        case 2:
          serverUpdates.current = [...serverUpdates.current, newData.text.redis_status];
          break;
        case 3:
          serverUpdates.current = [...serverUpdates.current, newData.text.grpc_status];
          break;
        default:
          break;
      }
      setForceRefresh(Math.random());
    };
    return eventSource;
  };

  useEffect(() => {
    const userCoordinatesEventSource = subscribeUserCoordinates(serverText.author_id);
    return () => {
      if (userCoordinatesEventSource) {
        userCoordinatesEventSource.close();
      }
    };
  }, []);

  useEffect(() => {
    const { data } = getRandom;
    if (data && data.getRandomText) {
      setServerText(data.getRandomText);
    }
  }, [getRandom]);

  const onInputChange = e => {
    const {
      target: { value },
    } = e;
    setText(value);
  };

  const checkEnter = e => {
    if (e.key === "Enter" || e.keyCode === 13) {
      serverUpdates.current = [];
      handleSubmit();
    }
  };

  const handleSubmit = () => {
    addText({
      variables: { message: newText, author_id: authorId },
    })
      .then(() => {
        setText("");
      })
      .catch(err => {
        if (err && err.graphQLErrors && err.graphQLErrors.length) {
          console.log("add fail", err.graphQLErrors[0].message);
        }
      });
  };

  const displayServerUpdates = () => {
    return (
      <div>
        {serverUpdates.current.map((update, index) => (
          <p key={index}>{update}</p>
        ))}
      </div>
    );
  };

  if (getRandom.loading) return <span>Loading...</span>;
  if (getRandom.error) return <span>Error</span>;

  return (
    <>
      <div
        style={{
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          height: "500px",
        }}
      >
        <img width="300px" alt="learn" src={learnImage} />
        <p style={{ margin: "20px" }}>{serverText.message}</p>
        <input
          style={{ width: "300px", marginBottom: "20px" }}
          value={newText}
          onChange={onInputChange}
          onKeyUp={checkEnter}
        />
        {serverUpdates.current.length ? displayServerUpdates() : null}
      </div>
    </>
  );
};

export default App;
